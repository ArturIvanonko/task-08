package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.XMLConstants;
import com.epam.rd.java.basic.task8.model.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;

	private Computers computers;

	private Computer computer;

	private Specifications specifications;

	private Processor processor;

	private Dimensions dimensions;

	private Graphics graphics;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse() throws ParserConfigurationException,
			SAXException, IOException, XMLStreamException {

		String currentElement = null;

		XMLInputFactory factory = XMLInputFactory.newInstance();

		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
				continue;
			}

			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();
				if(XMLConstants.COMPUTERS.equalsTo(currentElement)){
					computers = new Computers();
				}
				if(XMLConstants.COMPUTER.equalsTo(currentElement)){
					computer = new Computer();
				}
				if(XMLConstants.SPECIFICATIONS.equalsTo(currentElement)){
					specifications = new Specifications();
				}
				if(XMLConstants.PROCESSOR.equalsTo(currentElement)){
					processor = new Processor();
				}
				if(XMLConstants.DIMENSIONS.equalsTo(currentElement)){
					dimensions = new Dimensions();
				}
				if(XMLConstants.GRAPHICS.equalsTo(currentElement)){
					graphics = new Graphics();
				}
			}

			if (event.isCharacters()) {
				Characters characters = event.asCharacters();
				String elementText = characters.getData();
				if (XMLConstants.NAME.equalsTo(currentElement)) {
					computer.setName(elementText);
				}
				if (XMLConstants.PRODUCER.equalsTo(currentElement)) {
					computer.setProducer(elementText);
				}
				if (XMLConstants.TYPE.equalsTo(currentElement)) {
					computer.setType(elementText);
				}
				if (XMLConstants.YEAR.equalsTo(currentElement)) {
					computer.setYear(Integer.parseInt(elementText));
				}
				if (XMLConstants.PRICE.equalsTo(currentElement)) {
					specifications.setPrice(Integer.parseInt(elementText));
				}
				if (XMLConstants.COUNT_OF_CORES.equalsTo(currentElement)) {
					processor.setCountOfCores(Integer.parseInt(elementText));
				}
				if (XMLConstants.FREQUENCY.equalsTo(currentElement)) {
					processor.setFrequency(Integer.parseInt(elementText));
				}
				if (XMLConstants.LENGTH.equalsTo(currentElement)) {
					dimensions.setLength(Integer.parseInt(elementText));
				}
				if (XMLConstants.WIDTH.equalsTo(currentElement)) {
					dimensions.setWidth(Integer.parseInt(elementText));
				}
				if (XMLConstants.HEIGHT.equalsTo(currentElement)) {
					dimensions.setHeight(Integer.parseInt(elementText));
				}
				if (XMLConstants.VIDEO_MEMORY.equalsTo(currentElement)) {
					graphics.setVideoMemory(Double.parseDouble(elementText));
				}
				if (XMLConstants.COUNT_OF_COOLERS.equalsTo(currentElement)) {
					graphics.setCountOfCoolers(Double.parseDouble(elementText));
				}
				if (XMLConstants.MIN_POWER.equalsTo(currentElement)) {
					graphics.setMinPower(Double.parseDouble(elementText));
				}
			}

			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();

				if(XMLConstants.COMPUTER.equalsTo(localName)){
					computers.addCar(computer);
					computer = null;
				}
				if(XMLConstants.SPECIFICATIONS.equalsTo(localName)){
					computer.setSpecifications(specifications);
					specifications = null;
				}
				if(XMLConstants.PROCESSOR.equalsTo(localName)){
					specifications.setProcessor(processor);
					processor = null;
				}
				if(XMLConstants.DIMENSIONS.equalsTo(localName)){
					specifications.setDimensions(dimensions);
					dimensions = null;
				}
				if(XMLConstants.GRAPHICS.equalsTo(localName)){
					specifications.setGraphics(graphics);
					graphics = null;
				}
			}
		}
		reader.close();
	}

	public Computers getComputers() {
		return computers;
	}
}