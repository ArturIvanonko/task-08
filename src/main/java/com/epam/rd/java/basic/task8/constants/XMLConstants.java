package com.epam.rd.java.basic.task8.constants;

public enum XMLConstants {

    COMPUTERS("computers"),
        COMPUTER("computer"),
            NAME("name"),
            PRODUCER("producer"),
            TYPE("type"),
            YEAR("year"),

            SPECIFICATIONS("specifications"),
                PRICE("price"),

                PROCESSOR("processor"),
                    COUNT_OF_CORES("countOfCores"),
                    FREQUENCY("frequency"),

                DIMENSIONS("dimensions"),
                    LENGTH("length"),
                    WIDTH("width"),
                    HEIGHT("height"),


                GRAPHICS("graphics"),
                    VIDEO_MEMORY("videoMemory"),
                    COUNT_OF_COOLERS("countOfCoolers"),
                    MIN_POWER("minPower");

    private String value;

    XMLConstants(final String value) {
        this.value = value;
    }

    public boolean equalsTo(final String name) {
        return value.equals(name);
    }

    public final String value() {
        return value;
    }
}
