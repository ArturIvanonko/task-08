package com.epam.rd.java.basic.task8.model;


import java.util.ArrayList;
import java.util.List;

public class Computers {

    private List<Computer> computers;

    public Computers(){
        computers = new ArrayList<Computer>();
    }

    public final List<Computer> getComputers(){
        return this.computers;
    }

    public final void addCar(Computer car){
        computers.add(car);
    }

}
