package com.epam.rd.java.basic.task8.Utils;

import com.epam.rd.java.basic.task8.model.Computer;
import com.epam.rd.java.basic.task8.model.Computers;

import java.util.Comparator;

public final class Sorting {

    public static final Comparator<Computer>
            SORT_COMPUTER_BY_NAME = (first, second) -> {
        String name1 = first.getName();
        String name2 = second.getName();
        return name1.compareTo(name2);
    };

    public static final Comparator<Computer>
            SORT_COMPUTER_BY_PRICE = (first, second) -> {
        Integer price1 = first.getSpecifications().getPrice();
        Integer price2 = second.getSpecifications().getPrice();
        return price1.compareTo(price2);
    };

    public static final Comparator<Computer>
            SORT_COMPUTER_BY_PROCESSOR_FREQUENCY = (first, second) -> {
        Integer frequency1 = first.getSpecifications().getProcessor().getCountOfCores();
        Integer frequency2 = second.getSpecifications().getProcessor().getCountOfCores();
        return frequency1.compareTo(frequency2);
    };

    public static void sortComputersByName(Computers computers) {
        computers.getComputers().sort(SORT_COMPUTER_BY_NAME);
    }

    public static void sortComputersByPrice(Computers computers) {
        computers.getComputers().sort(SORT_COMPUTER_BY_PRICE);
    }

    public static void sortComputersByProcessorFrequency(Computers cars) {
        cars.getComputers().sort(SORT_COMPUTER_BY_PROCESSOR_FREQUENCY);
    }

    private Sorting() {

    }


}
