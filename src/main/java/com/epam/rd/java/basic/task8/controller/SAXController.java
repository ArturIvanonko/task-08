package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.DocumentConstants;
import com.epam.rd.java.basic.task8.constants.XMLConstants;
import com.epam.rd.java.basic.task8.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private final String xmlFileName;

	private String currentElement;

	private Computers computers;

	private Computer computer;

	private Specifications specifications;

	private Processor processor;

	private Dimensions dimensions;

	private Graphics graphics;




	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public final void parse( boolean validate)
			throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		if (validate) {
			factory.setFeature(DocumentConstants.TURN_VALIDATION_ON, true);
			factory.setFeature(DocumentConstants.TURN_SCHEMA_VALIDATION_ON, true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		currentElement = localName;
		if(XMLConstants.COMPUTERS.equalsTo(currentElement)){
			computers = new Computers();
			return;
		}
		if(XMLConstants.COMPUTER.equalsTo(currentElement)){
			computer = new Computer();
			return;
		}
		if(XMLConstants.SPECIFICATIONS.equalsTo(currentElement)){
			specifications = new Specifications();
			return;
		}
		if(XMLConstants.PROCESSOR.equalsTo(currentElement)){
			processor = new Processor();
			return;
		}
		if(XMLConstants.DIMENSIONS.equalsTo(currentElement)){
			dimensions = new Dimensions();
			return;
		}
		if(XMLConstants.GRAPHICS.equalsTo(currentElement)){
			graphics = new Graphics();
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String elementText = new String(ch, start, length).trim();

		if (elementText.isEmpty()) {
			return;
		}
		if (XMLConstants.NAME.equalsTo(currentElement)) {
			computer.setName(elementText);
			return;
		}
		if (XMLConstants.PRODUCER.equalsTo(currentElement)) {
			computer.setProducer(elementText);
			return;
		}
		if (XMLConstants.TYPE.equalsTo(currentElement)) {
			computer.setType(elementText);
			return;
		}
		if (XMLConstants.YEAR.equalsTo(currentElement)) {
			computer.setYear(Integer.parseInt(elementText));
			return;
		}
		if (XMLConstants.PRICE.equalsTo(currentElement)) {
			specifications.setPrice(Integer.parseInt(elementText));
			return;
		}
		if (XMLConstants.COUNT_OF_CORES.equalsTo(currentElement)) {
			processor.setCountOfCores(Integer.parseInt(elementText));
			return;
		}
		if (XMLConstants.FREQUENCY.equalsTo(currentElement)) {
			processor.setFrequency(Integer.parseInt(elementText));
			return;
		}
		if (XMLConstants.LENGTH.equalsTo(currentElement)) {
			dimensions.setLength(Integer.parseInt(elementText));
			return;
		}
		if (XMLConstants.WIDTH.equalsTo(currentElement)) {
			dimensions.setWidth(Integer.parseInt(elementText));
			return;
		}
		if (XMLConstants.HEIGHT.equalsTo(currentElement)) {
			dimensions.setHeight(Integer.parseInt(elementText));
			return;
		}

		if (XMLConstants.VIDEO_MEMORY.equalsTo(currentElement)) {
			graphics.setVideoMemory(Double.parseDouble(elementText));
			return;
		}
		if (XMLConstants.COUNT_OF_COOLERS.equalsTo(currentElement)) {
			graphics.setCountOfCoolers(Double.parseDouble(elementText));
			return;
		}
		if (XMLConstants.MIN_POWER.equalsTo(currentElement)) {
			graphics.setMinPower(Double.parseDouble(elementText));
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(XMLConstants.COMPUTER.equalsTo(localName)){
			computers.addCar(computer);
			computer = null;
			return;
		}
		if(XMLConstants.SPECIFICATIONS.equalsTo(localName)){
			computer.setSpecifications(specifications);
			specifications = null;
			return;
		}
		if(XMLConstants.PROCESSOR.equalsTo(localName)){
			specifications.setProcessor(processor);
			processor = null;
			return;
		}
		if(XMLConstants.DIMENSIONS.equalsTo(localName)){
			specifications.setDimensions(dimensions);
			dimensions = null;
			return;
		}
		if(XMLConstants.GRAPHICS.equalsTo(localName)){
			specifications.setGraphics(graphics);
			graphics = null;
		}
	}

	public Computers getComputers() {
		return computers;
	}

	public void setComputers(Computers computers) {
		this.computers = computers;
	}
}