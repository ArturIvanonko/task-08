package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.DocumentConstants;
import com.epam.rd.java.basic.task8.constants.XMLConstants;
import com.epam.rd.java.basic.task8.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	private Computers computers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	// PLACE YOUR CODE HERE

	public final void parse(boolean validate) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		if(validate){
			dbf.setFeature(DocumentConstants.TURN_VALIDATION_ON,true);
			dbf.setFeature(DocumentConstants.TURN_SCHEMA_VALIDATION_ON,true);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();

		Document document = db.parse(xmlFileName);

		Element root = document.getDocumentElement();

		computers = new Computers();

		NodeList carNodes = root.getElementsByTagName(XMLConstants.COMPUTER.value());

		for (int i = 0; i < carNodes.getLength(); i++) {
			Computer car = getCar(carNodes.item(i));
			computers.addCar(car);
		}

	}

	private Computer getCar(Node node) {
		Computer computer = new Computer();
		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XMLConstants.NAME.value());
		computer.setName(dNode.item(0).getTextContent());

		dNode = element.getElementsByTagName(XMLConstants.PRODUCER.value());
		computer.setProducer(dNode.item(0).getTextContent());

		dNode = element.getElementsByTagName(XMLConstants.TYPE.value());
		computer.setType(dNode.item(0).getTextContent());

		dNode = element.getElementsByTagName(XMLConstants.YEAR.value());
		computer.setYear(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XMLConstants.SPECIFICATIONS.value());
		computer.setSpecifications(getSpecifications(dNode.item(0)));

		System.out.println(computer);
		return computer;
	}

	private Specifications getSpecifications(Node node) {
		Specifications specifications = new Specifications();

		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XMLConstants.PRICE.value());
		specifications.setPrice(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XMLConstants.PROCESSOR.value());
		specifications.setProcessor(getEngine(dNode.item(0)));

		dNode = element.getElementsByTagName(XMLConstants.DIMENSIONS.value());
		specifications.setDimensions(getDimensions(dNode.item(0)));

		dNode = element.getElementsByTagName(XMLConstants.GRAPHICS.value());
		specifications.setGraphics(getFuelConsumptions(dNode.item(0)));


		return specifications;
	}

	private Processor getEngine(Node node) {
		Processor processor = new Processor();
		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XMLConstants.COUNT_OF_CORES.value());
		processor.setCountOfCores(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XMLConstants.FREQUENCY.value());
		processor.setFrequency(Integer.parseInt(dNode.item(0).getTextContent()));

		return processor;

	}

	private Dimensions getDimensions(Node node) {
		Dimensions dimensions = new Dimensions();
		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XMLConstants.LENGTH.value());
		dimensions.setLength(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XMLConstants.WIDTH.value());
		dimensions.setWidth(Integer.parseInt(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XMLConstants.HEIGHT.value());
		dimensions.setHeight(Integer.parseInt(dNode.item(0).getTextContent()));

//		dNode = element.getElementsByTagName(XML.PASSENGER_VOLUME.value());
//		dimensions.setVideoCardSlots(Integer.parseInt(dNode.item(0).getTextContent()));
//
//		dNode = element.getElementsByTagName(XML.CARGO_VOLUME.value());
//		dimensions.setCargoVolume(Integer.parseInt(dNode.item(0).getTextContent()));

		return dimensions;
	}

	private Graphics getFuelConsumptions(Node node) {
		Graphics graphics = new Graphics();
		Element element = (Element) node;

		NodeList dNode = element.getElementsByTagName(XMLConstants.VIDEO_MEMORY.value());
		graphics.setVideoMemory(Double.parseDouble(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XMLConstants.COUNT_OF_COOLERS.value());
		graphics.setCountOfCoolers(Double.parseDouble(dNode.item(0).getTextContent()));

		dNode = element.getElementsByTagName(XMLConstants.MIN_POWER.value());
		graphics.setMinPower(Double.parseDouble(dNode.item(0).getTextContent()));

		return graphics;
	}

	public static Document getDocument(Computers cars) throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		Element root = document.createElement(XMLConstants.COMPUTERS.value());
		root.setAttribute("xmlns", DocumentConstants.XMLNS);
		root.setAttribute("xmlns:xsi", DocumentConstants.XMLNS_XSI);
		root.setAttribute("xsi:schemaLocation", DocumentConstants.XSI_SCHEMA_LOCATION);
		document.appendChild(root);

		for(Computer computer : cars.getComputers()){

			Element computerElement = document.createElement(XMLConstants.COMPUTER.value());
			root.appendChild(computerElement);

			Element nameElement = document.createElement(XMLConstants.NAME.value());
			nameElement.setTextContent(computer.getName());
			computerElement.appendChild(nameElement);

			Element originElement = document.createElement(XMLConstants.PRODUCER.value());
			originElement.setTextContent(computer.getProducer());
			computerElement.appendChild(originElement);

			Element typeElement = document.createElement(XMLConstants.TYPE.value());
			typeElement.setTextContent(computer.getType());
			computerElement.appendChild(typeElement);

			Element yearElement = document.createElement(XMLConstants.YEAR.value());
			yearElement.setTextContent(String.valueOf(computer.getYear()));
			computerElement.appendChild(yearElement);

			//--------------------------------------------------------------------------------
			Element specificationsElement = document.createElement(XMLConstants.SPECIFICATIONS.value());
			computerElement.appendChild(specificationsElement);

			final String computerPrice = String.valueOf(computer.getSpecifications().getPrice());
			Element priceElement = document.createElement(XMLConstants.PRICE.value());
			priceElement.setTextContent(computerPrice);
			specificationsElement.appendChild(priceElement);

			Element processorElement = document.createElement(XMLConstants.PROCESSOR.value());
			specificationsElement.appendChild(processorElement);

			final String countOfCores = String.valueOf(computer.getSpecifications().getProcessor().getCountOfCores());
			final String frequency = String.valueOf(computer.getSpecifications().getProcessor().getFrequency());

			Element countOfCoresElement = document.createElement(XMLConstants.COUNT_OF_CORES.value());
			countOfCoresElement.setTextContent(countOfCores);
			processorElement.appendChild(countOfCoresElement);

			Element frequencyElement = document.createElement(XMLConstants.FREQUENCY.value());
			frequencyElement.setTextContent(frequency);
			processorElement.appendChild(frequencyElement);

			Element dimensionsElement = document.createElement(XMLConstants.DIMENSIONS.value());
			specificationsElement.appendChild(dimensionsElement);

			final String carLength = String.valueOf(computer.getSpecifications().getDimensions().getLength());
			final String carWidth = String.valueOf(computer.getSpecifications().getDimensions().getWidth());
			final String carHeight = String.valueOf(computer.getSpecifications().getDimensions().getHeight());

			Element lengthElement = document.createElement(XMLConstants.LENGTH.value());
			lengthElement.setTextContent(carLength);
			dimensionsElement.appendChild(lengthElement);

			Element widthElement = document.createElement(XMLConstants.WIDTH.value());
			widthElement.setTextContent(carWidth);
			dimensionsElement.appendChild(widthElement);

			Element heightElement = document.createElement(XMLConstants.HEIGHT.value());
			heightElement.setTextContent(carHeight);
			dimensionsElement.appendChild(heightElement);


			Element graphicElement = document.createElement(XMLConstants.GRAPHICS.value());
			specificationsElement.appendChild(graphicElement);

			final String videoMemory = String.valueOf(computer.getSpecifications().getGraphics().getVideoMemory());
			final String countOfCoolers = String.valueOf(computer.getSpecifications().getGraphics().getCountOfCoolers());
			final String minPower = String.valueOf(computer.getSpecifications().getGraphics().getMinPower());

			Element videoMemoryElement = document.createElement(XMLConstants.VIDEO_MEMORY.value());
			videoMemoryElement.setTextContent(videoMemory);
			graphicElement.appendChild(videoMemoryElement);

			Element countOfCoolersElement = document.createElement(XMLConstants.COUNT_OF_COOLERS.value());
			countOfCoolersElement.setTextContent(countOfCoolers);
			graphicElement.appendChild(countOfCoolersElement);

			Element minPowerElement = document.createElement(XMLConstants.MIN_POWER.value());
			minPowerElement.setTextContent(minPower);
			graphicElement.appendChild(minPowerElement);
		}

		return document;
	}

	public static void saveToXML(Computers cars, String xmlFileName) throws ParserConfigurationException, TransformerException {
		saveToXML(getDocument(cars),xmlFileName);
	}

	public static void saveToXML(Document document,String xmlFileName) throws TransformerException {
		StreamResult result = new StreamResult(new File(xmlFileName));
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT,"yes");
		transformer.transform(new DOMSource(document),result);
	}

	public Computers getComputers() {
		return computers;
	}
}
