package com.epam.rd.java.basic.task8.model;

public class Specifications {

    private int price;

    private Processor processor;

    private Dimensions dimensions;

    private Graphics graphics;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }

    public Graphics getGraphics() {
        return graphics;
    }

    public void setGraphics(Graphics graphics) {
        this.graphics = graphics;
    }

    @Override
    public String toString() {
        return
                "\n\t\tprice=" + price +
                "\n\t\tprocessor=" + processor +
                "\n\t\tdimensions=" + dimensions +
                "\n\t\tgraphics=" + graphics +"\n";
    }
}
