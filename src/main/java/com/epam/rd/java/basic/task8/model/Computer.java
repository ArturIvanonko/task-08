package com.epam.rd.java.basic.task8.model;

public class Computer {
    private String name;

    private String producer;

    private String type;

    private int year;

    private Specifications specifications;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String origin) {
        this.producer = origin;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Specifications getSpecifications() {
        return specifications;
    }

    public void setSpecifications(Specifications specifications) {
        this.specifications = specifications;
    }

    @Override
    public String toString() {
        return "Car" +
                "\n\tname=" + name +
                "\n\tproducer=" + producer +
                "\n\ttype=" + type +
                "\n\tyear=" + year +
                "\n\tspecifications" + specifications + "\n";
    }
}
