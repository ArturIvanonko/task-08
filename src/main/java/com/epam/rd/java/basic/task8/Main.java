package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.Utils.Sorting;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Computers;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		try {
			////////////////////////////////////////////////////////
			// DOM
			////////////////////////////////////////////////////////
			// get container
			DOMController domController = new DOMController(xmlFileName);
			// PLACE YOUR CODE HERE
			domController.parse(true);
			Computers computers = domController.getComputers();
			// sort (case 1)
			// PLACE YOUR CODE HERE
			Sorting.sortComputersByName(computers);
			// save
			String outputXmlFile = "output.dom.xml";
			// PLACE YOUR CODE HERE
			DOMController.saveToXML(computers,outputXmlFile);
			////////////////////////////////////////////////////////
			// SAX
			////////////////////////////////////////////////////////

			// get
			SAXController saxController = new SAXController(xmlFileName);
			// PLACE YOUR CODE HERE
			saxController.parse(true);
			computers = saxController.getComputers();
			// sort  (case 2)
			// PLACE YOUR CODE HERE
			Sorting.sortComputersByPrice(computers);
			// save
			outputXmlFile = "output.sax.xml";
			// PLACE YOUR CODE HERE
			DOMController.saveToXML(computers,outputXmlFile);
			////////////////////////////////////////////////////////
			// StAX
			////////////////////////////////////////////////////////

			// get
			STAXController staxController = new STAXController(xmlFileName);
			// PLACE YOUR CODE HERE
			staxController.parse();
			computers = staxController.getComputers();
			// sort  (case 3)
			// PLACE YOUR CODE HERE
			Sorting.sortComputersByProcessorFrequency(computers);
			// save
			outputXmlFile = "output.stax.xml";
			// PLACE YOUR CODE HERE
			DOMController.saveToXML(computers,outputXmlFile);
		} catch (ParserConfigurationException e) {
			System.err.println(e.getMessage());
		} catch (SAXException e) {
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} catch (TransformerException e) {
			System.err.println(e.getMessage());
		} catch (XMLStreamException e) {
			System.err.println(e.getMessage());
		}
	}

}