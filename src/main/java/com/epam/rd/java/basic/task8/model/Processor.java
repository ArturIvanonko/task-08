package com.epam.rd.java.basic.task8.model;

public class Processor {

    private int countOfCores;
    private int frequency;

    public int getCountOfCores() {
        return countOfCores;
    }

    public void setCountOfCores(int countOfCores) {
        this.countOfCores = countOfCores;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "power=" + countOfCores +
                ", torque=" + frequency +
                '}';
    }
}
