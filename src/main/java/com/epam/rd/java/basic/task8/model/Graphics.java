package com.epam.rd.java.basic.task8.model;

public class Graphics {
    private double videoMemory;
    private double countOfCoolers;
    private double minPower;

    public double getVideoMemory() {
        return videoMemory;
    }

    public void setVideoMemory(double videoMemory) {
        this.videoMemory = videoMemory;
    }

    public double getCountOfCoolers() {
        return countOfCoolers;
    }

    public void setCountOfCoolers(double countOfCoolers) {
        this.countOfCoolers = countOfCoolers;
    }

    public double getMinPower() {
        return minPower;
    }

    public void setMinPower(double minPower) {
        this.minPower = minPower;
    }

    @Override
    public String toString() {
        return "FuelConsumptions{" +
                "general=" + videoMemory +
                ", city=" + countOfCoolers +
                ", highway=" + minPower +
                '}';
    }
}
